const db = require('../model/db')
const Base = require('./base')

const TableName = 'tk_about'

class About extends Base {
  // 列表
  async findAll(req, res) {
    const { title, page, limit } = req.query
    const currentPage = parseInt(page || 1)
    const pageSize = parseInt(limit || 10)
    const startIndex = (currentPage - 1) * pageSize
    const app_id = req.headers.appid || 1
    const data = { total: 0, rows: {} }
    const sql1 = `select * from ${TableName} where app_id = ${app_id}`

    const [[rows]] = await db.query(sql1, [`%${title || ''}%`, startIndex, pageSize])

    if (rows) {
      data.rows = rows
    }

    return res.cc('success', 0, { data })
  }

  // 详情
  async findOne(req, res) {
    const data = { total: 0, rows: '' }
    const app_id = req.headers.appid || 1
    const sql = `select * from ${TableName} where app_id=${app_id}`

    const [[rows]] = await db.query(sql)

    if (rows) {
      data.rows = rows
      delete rows.password
    }

    return res.cc('success', 0, { data })
  }

  async create(req, res) {
    await this.baseCreate(req, res)
  }

  async update(req, res) {
    await this.baseUpdate(req, res, 'app_id')
  }

  async remove(req, res) {
    const app_id = req.headers.appid

    if (!app_id) {
      return res.cc('appid参数不能为空')
    }

    await this.baseRemove(req, res, { key: 'app_id', value: app_id })
  }
}

module.exports = About
