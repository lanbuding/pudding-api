const db = require('../model/db')

const TableName = 'tk_article'
const Home = {
  // 列表
  async findAll(req, res) {
    const { title, page, limit } = req.query
    const currentPage = parseInt(page || 1)
    const pageSize = parseInt(limit || 10)
    const startIndex = (currentPage - 1) * pageSize
    const app_id = req.headers.appid || 1
    const data = { total: 0, rows: [] }
    const sql1 = `select * from ${TableName} where app_id = ${app_id} and content LIKE ? LIMIT ?, ?`
    const sql2 = `select count(*) as total from ${TableName} where title LIKE ? and app_id = ${app_id}`

    const [rows] = await db.query(sql1, [`%${title || ''}%`, startIndex, pageSize])
    const [[{ total }]] = await db.query(sql2, [`%${title || ''}%`])

    if (rows.length > 0) {
      data.rows = rows
      data.total = total
    }

    return res.cc('success', 0, { data })
  },
}

module.exports = Home
