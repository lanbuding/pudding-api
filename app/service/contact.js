const Base = require('./base')

class Contact extends Base {
  async findAll(req, res) {
    const data = { a: 1, b: 2 }

    this.getModelName()

    return res.cc('success', 0, { data })
  }

  async findOne(req, res) {
    console.log('req.headers', req.headers)
    const app_id = req.headers.appid || 1

    await this.baseFindOne({ req, res, where: `app_id = ${app_id}` })
  }

  async create(req, res) {
    await this.baseCreate(req, res)
  }

  async update(req, res) {
    await this.baseUpdate(req, res)
  }

  async remove(req, res) {
    await this.baseRemove(req, res)
  }
}

module.exports = Contact
