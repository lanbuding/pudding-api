const db = require('../model/db')

const TableName = 'tk_banner'

const Base = require('./base')

class Banner extends Base {
  // 列表
  async findAll(req, res) {
    const { title, page, limit } = req.query
    const { id: modelId } = req.params
    const currentPage = parseInt(page || 1)
    const pageSize = parseInt(limit || 10)
    const startIndex = (currentPage - 1) * pageSize
    const app_id = req.headers.appid || 1
    const data = { total: 0, rows: [] }
    const mId = modelId ? `= ${modelId}` : 'is not null'
    const sql1 = `select * from ${TableName} where app_id = ${app_id} and title LIKE ? and modelId ${mId} order by id desc LIMIT ?, ?`
    const sql2 = `select count(*) as total from ${TableName} where title LIKE ? and modelId ${mId} and app_id = ${app_id}`


    const [rows] = await db.query(sql1, [`%${title || ''}%`, startIndex, pageSize])
    const [[{ total }]] = await db.query(sql2, [`%${title || ''}%`])

    if (rows.length > 0) {
      data.rows = rows
      data.total = total
    }

    return res.cc('success', 0, { data })
  }

  // 详情
  async findOne(req, res) {
    const { id } = req.params
    const data = { total: 0, rows: '' }
    const sql = `select * from ${TableName} where id = ?`

    const [[rows]] = await db.query(sql, [id])

    if (rows) {
      data.rows = rows
      delete rows.password
    }

    return res.cc('success', 0, { data })
  }

  async create(req, res) {
    await this.baseCreate(req, res)
  }

  async update(req, res) {
    await this.baseUpdate(req, res)
  }

  async remove(req, res) {
    await this.baseRemove(req, res)
  }
}

module.exports = Banner
