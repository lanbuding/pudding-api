const db = require('../model/db')
const Base = require('./base')

const TableName = 'tk_system'

class System extends Base {
  // 列表
  async findAll(req, res) {
    const { title, page = 1, limit = 10 } = req.query
    const startIndex = (page - 1) * limit
    const app_id = req.headers.appid || 1
    const data = { total: 0, rows: [] }
    const sql1 = `select * from ${TableName} where app_id = ${app_id} and email LIKE ? LIMIT ?, ?`

    const [rows] = await db.query(sql1, [`%${title || ''}%`, startIndex, Number(limit)])

    if (rows.length > 0) {
      data.rows = rows
    }

    return res.cc('success', 0, { data })
  }

  async findOne(req, res) {
    await this.baseFindOne({ req, res })
  }

  async create(req, res) {
    await this.baseCreate(req, res)
  }

  async update(req, res) {
    await this.baseUpdate(req, res, 'app_id')
  }

  async remove(req, res) {
    const app_id = req.headers.appid

    if (!app_id) {
      return res.cc('appid参数不能为空')
    }

    await this.baseRemove(req, res, { key: 'app_id', value: app_id })
  }
}

module.exports = System
