const User = require('./user')
const Article = require('./article')
const Category = require('./category')
const Message = require('./message')
const Comment = require('./comment')
const System = require('./system')
const About = require('./about')
const Link = require('./link')
const Banner = require('./banner')
const Home = require('./home')
const file = require('./file')
const Contact = require('./contact')

module.exports = {
  User,
  Article,
  Category,
  Message,
  Comment,
  System,
  About,
  Link,
  Banner,
  Home,
  file,
  Contact,
}
