const db = require('../model/db')
const Base = require('./base')

const TableName = 'tk_comment'

class Comment extends Base {
  // 列表
  async findAll(req, res) {
    const { title, page, limit } = req.query
    const currentPage = parseInt(page || 1)
    const pageSize = parseInt(limit || 10)
    const startIndex = (currentPage - 1) * pageSize
    const app_id = req.headers.appid || 1
    const data = { total: 0, rows: [] }
    const sql1 = `select * from ${TableName} where app_id = ${app_id} and content LIKE ? order by id desc LIMIT ?, ?`
    const sql2 = `select count(*) as total from ${TableName} where content LIKE ? and app_id = ${app_id}`

    const [rows] = await db.query(sql1, [`%${title || ''}%`, startIndex, pageSize])
    const [[{ total }]] = await db.query(sql2, [`%${title || ''}%`])

    if (rows.length > 0) {
      data.rows = rows
      data.total = total
    }

    return res.cc('success', 0, { data })
  }

  // 详情
  async findOne(req, res) {
    const { id } = req.params
    const data = { total: 0, rows: '' }
    const sql = `select * from ${TableName} where id = ?`

    const [[rows]] = await db.query(sql, [id])

    if (rows) {
      data.rows = rows
      delete rows.password
    }

    return res.cc('success', 0, { data })
  }

  // 详情
  async findOneByArticleId(req, res) {
    const { title, id } = req.params
    const data = { total: 0, rows: '' }
    const app_id = req.headers.appid || 1
    const sql = `select * from ${TableName} where bid = ? order by id desc `
    const sql2 = `select count(*) as total from ${TableName} where content LIKE ? and app_id = ${app_id}`

    const [rows] = await db.query(sql, [id])
    const [[{ total }]] = await db.query(sql2, [`%${title || ''}%`])

    if (rows) {
      data.rows = rows
      data.total = total
      delete rows.password
    }

    return res.cc('success', 0, { data })
  }

  // 新增
  async create(req, res) {
    // const { verify } = req.body
    const sql = `insert into ${TableName} set ?`
    const app_id = req.headers.appid || 1

    // if (verify !== req.session.verify) {
    //   return res.cc('验证码不正确', 1)
    // }

    delete req.session.verify

    req.body.app_id = app_id

    const [rows] = await db.query(sql, [req.body])

    if (!rows.affectedRows) {
      return res.cc('新增失败！')
    }

    return res.cc('success', 0)
  }

  async update(req, res) {
    await this.baseUpdate(req, res)
  }

  async remove(req, res) {
    await this.baseRemove(req, res)
  }
}

module.exports = Comment
