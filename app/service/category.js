const db = require('../model/db')
const Base = require('./base')

const TableName = 'tk_category'

class Category extends Base {
  // 列表
  async findAll(req, res) {
    const { title, page, limit, modelid } = req.query
    const currentPage = parseInt(page || 1)
    const pageSize = parseInt(limit || 10)
    const startIndex = (currentPage - 1) * pageSize
    const app_id = req.headers.appid || 1
    const data = { total: 0, rows: [] }

    const modelId = Number(modelid) ? `= ${modelid}` : 'is not null'
    const sql1 = `select * from ${TableName} where app_id = ${app_id} and modelid ${modelId} and colTitle LIKE ? order by id desc LIMIT ?, ?`
    const sql2 = `select count(*) as total from ${TableName} where colTitle LIKE ? and app_id = ${app_id} and modelid ${modelId}`

    const [rows] = await db.query(sql1, [`%${title || ''}%`, startIndex, pageSize])
    const [[{ total }]] = await db.query(sql2, [`%${title || ''}%`])

    if (rows.length > 0) {
      data.rows = rows
      data.total = total
    }

    return res.cc('success', 0, { data })
  }

  async findOne(req, res) {
    await this.baseFindOne({ req, res })
  }

  async create(req, res) {
    await this.baseCreate(req, res)
  }

  async update(req, res) {
    await this.baseUpdate(req, res)
  }

  async remove(req, res) {
    await this.baseRemove(req, res)
  }
}

module.exports = Category
