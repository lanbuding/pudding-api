const svgCaptcha = require('svg-captcha')

const CommonController = {
  captcha(req, res) {
    const svg = svgCaptcha.create({
      color: true, // 彩色
      // inverse:false,// 反转颜色
      width: 100, //  宽度
      height: 40, // 高度
      fontSize: 48, // 字体大小
      size: 4, // 验证码的长度
      noise: 3, // 干扰线条
      ignoreChars: '0oO1ilI', // 验证码字符中排除 0o1i
    })

    req.session.verify = svg.text.toLowerCase()
    console.log('req.session.verify', req.session.verify)
    res.type('svg')
    res.send(svg.data)
  },
}

module.exports = CommonController
