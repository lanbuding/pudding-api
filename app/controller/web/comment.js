const service = require('root/app/service')

const comment = new service.Comment()
const CommentController = {
  async findAll(req, res) {
    return await comment.findAll(req, res)
  },
  async findOne(req, res) {
    return await comment.findOne(req, res)
  },
  async findOneByArticleId(req, res) {
    return await comment.findOneByArticleId(req, res)
  },
  async create(req, res) {
    return await comment.create(req, res)
  },
}

module.exports = CommentController
