const service = require('root/app/service')

const user = new service.User()
const UserController = {
  async findAll(req, res) {
    return await user.findAll(req, res)
  },
  async findOne(req, res) {
    return await user.findOne(req, res)
  },
  async getOne(req, res) {
    return await user.getOne(req, res)
  },
}

module.exports = UserController


