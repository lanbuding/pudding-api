const service = require('root/app/service')

const article = new service.Article()
const banner = new service.Banner()
const category = new service.Category()
const link = new service.Link()
const user = new service.User()
const HomeController = {
  async findAll(req, res) {
    return await article.findAll(req, res)
  },
  async findArticle(req, res) {
    return await article.findAll(req, res)
  },
  async findBanner(req, res) {
    return await banner.findAll(req, res)
  },
  async findUserInfo(req, res) {
    return await user.findOne(req, res)
  },
  async findCategory(req, res) {
    return await category.findAll(req, res)
  },
  async findLink(req, res) {
    return await link.findAll(req, res)
  },
}

module.exports = HomeController
