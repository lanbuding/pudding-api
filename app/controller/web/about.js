const service = require('root/app/service')

const about = new service.About()
const AboutController = {
  async findAll(req, res) {
    return await about.findAll(req, res)
  },
}

module.exports = AboutController
