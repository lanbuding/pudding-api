const service = require('root/app/service')
const article = new service.Article()
const category = new service.Category()

const ArticleController = {
  async findOne(req, res) {
    return await article.findOne(req, res)
  },
  async findCategory(req, res) {
    return await category.findAll(req, res)
  },
  async like(req, res) {
    return await article.like(req, res)
  },
}

module.exports = ArticleController
