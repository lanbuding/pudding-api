const service = require('root/app/service')

const message = new service.Message()
const MessageController = {
  async findAll(req, res) {
    return await message.findAll(req, res)
  },
  async create(req, res) {
    return await message.create(req, res)
  },
}

module.exports = MessageController
