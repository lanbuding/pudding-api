const user = require('./user')
const home = require('./home')
const article = require('./article')
const message = require('./message')
const about = require('./about')
const common = require('./common')
const comment = require('./comment')
const contact = require('./contact')

module.exports = {
  user,
  home,
  article,
  message,
  about,
  common,
  comment,
  contact,
}


