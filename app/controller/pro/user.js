const service = require('root/app/service')

const user = new service.User()
const UserController = {
  // 登录
  async login(req, res) {
    return await user.login(req, res)
  },
  // 注册
  async register(req, res) {
    return await user.register(req, res)
  },
  // 退出
  async logout(req, res) {
    req.session.userId = null

    return res.json({ code: 1, msg: '已退出' })
  },
  async findAll(req, res) {
    return await user.findAll(req, res)
  },
  async findOne(req, res) {
    return await user.findOne(req, res)
  },
  async create(req, res) {
    return await user.create(req, res)
  },
  async update(req, res) {
    return await user.update(req, res)
  },
  async remove(req, res) {
    return await user.remove(req, res)
  },
  async changePassword(req, res) {
    return await user.changePassword(req, res)
  },
}

// function md5Pwd(pwd) {
//   const salt = config.secretKey

//   return utils.md5(utils.md5(pwd + salt))
// }

module.exports = UserController


