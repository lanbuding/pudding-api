const service = require('root/app/service')
const article = new service.Article()

const ArticleController = {
  async findAll(req, res) {
    return await article.findAll(req, res)
  },
  async findOne(req, res) {
    return await article.findOne(req, res)
  },
  async create(req, res) {
    return await article.create(req, res)
  },
  async update(req, res) {
    return await article.update(req, res)
  },
  async remove(req, res) {
    return await article.remove(req, res)
  },
}

module.exports = ArticleController


