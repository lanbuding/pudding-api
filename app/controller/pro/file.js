const service = require('root/app/service')

const FileController = {
  async upload(req, res) {
    return await service.file.upload(req, res)
  },
}

module.exports = FileController


