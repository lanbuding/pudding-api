const service = require('root/app/service')

const category = new service.Category()

const CategoryController = {
  async findAll(req, res) {
    return await category.findAll(req, res)
  },
  async findOne(req, res) {
    return await category.findOne(req, res)
  },
  async create(req, res) {
    return await category.create(req, res)
  },
  async update(req, res) {
    return await category.update(req, res)
  },
  async remove(req, res) {
    return await category.remove(req, res)
  },
}

module.exports = CategoryController


