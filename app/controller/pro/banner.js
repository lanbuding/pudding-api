const service = require('root/app/service')

const banner = new service.Banner()
const BannerController = {
  async findAll(req, res) {
    return await banner.findAll(req, res)
  },
  async findOne(req, res) {
    return await banner.findOne(req, res)
  },
  async create(req, res) {
    return await banner.create(req, res)
  },
  async update(req, res) {
    return await banner.update(req, res)
  },
  async remove(req, res) {
    return await banner.remove(req, res)
  },
}

module.exports = BannerController


