const user = require('./user')
const article = require('./article')
const category = require('./category')
const message = require('./message')
const comment = require('./comment')
const system = require('./system')
const about = require('./about')
const link = require('./link')
const banner = require('./banner')
const file = require('./file')
const contact = require('./contact')

module.exports = {
  user,
  article,
  category,
  message,
  comment,
  system,
  about,
  link,
  banner,
  file,
  contact,
}
