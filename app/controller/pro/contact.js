const service = require('root/app/service')

const contact = new service.Contact()
const ContactController = {
  async findAll(req, res) {
    return await contact.findAll(req, res)
  },
  async findOne(req, res) {
    return await contact.findOne(req, res)
  },
  async create(req, res) {
    return await contact.create(req, res)
  },
  async update(req, res) {
    return await contact.update(req, res)
  },
  async remove(req, res) {
    return await contact.remove(req, res)
  },
}

module.exports = ContactController


