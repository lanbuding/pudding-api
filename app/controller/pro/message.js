const service = require('root/app/service')

const message = new service.Message()

const MessageController = {
  async findAll(req, res) {
    return await message.findAll(req, res)
  },
  async findOne(req, res) {
    return await message.findOne(req, res)
  },
  async create(req, res) {
    return await message.create(req, res)
  },
  async update(req, res) {
    return await message.update(req, res)
  },
  async remove(req, res) {
    return await message.remove(req, res)
  },
}

module.exports = MessageController


