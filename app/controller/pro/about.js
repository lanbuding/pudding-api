const service = require('root/app/service')

const about = new service.About()
const AboutController = {
  async findAll(req, res) {
    return await about.findAll(req, res)
  },
  async findOne(req, res) {
    return await about.findOne(req, res)
  },
  async create(req, res) {
    return await about.create(req, res)
  },
  async update(req, res) {
    return await about.update(req, res)
  },
  async remove(req, res) {
    return await about.remove(req, res)
  },
}

module.exports = AboutController


