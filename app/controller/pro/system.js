const service = require('root/app/service')

const system = new service.System()

const SystemController = {
  async findAll(req, res) {
    return await system.findAll(req, res)
  },
  async findOne(req, res) {
    return await system.findOne(req, res)
  },
  async create(req, res) {
    return await system.create(req, res)
  },
  async update(req, res) {
    return await system.update(req, res)
  },
  async remove(req, res) {
    return await system.remove(req, res)
  },
}

module.exports = SystemController


