const service = require('root/app/service')
const link = new service.Link()

const LinkController = {
  async findAll(req, res) {
    return await link.findAll(req, res)
  },
  async findOne(req, res) {
    return await link.findOne(req, res)
  },
  async create(req, res) {
    return await link.create(req, res)
  },
  async update(req, res) {
    return await link.update(req, res)
  },
  async remove(req, res) {
    return await link.remove(req, res)
  },
}

module.exports = LinkController


