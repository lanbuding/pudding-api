module.exports = {
  theme: 'default',
  keywords: '网上购物,网上商城,手机,笔记本,电脑,相机,数码,配件,手表,存储卡,微燕',
  title: '微燕(vyan.top)-正品低价、品质保障、配送及时、轻松购物！',
  description: '微燕(vyan.top)-专业的综合网上购物商城,销售家电、数码通讯、电脑、家居百货、服装服饰、母婴等数万个品牌优质商品.便捷、诚信的服务，为您提供愉悦的网上购物体验!',
  email: 'jikeytang@163.com',
  contact: 'Admin',
  company: '微燕科技',
  phone: '021-1234561551',
  icp: '',
  address: '中国 • 上海',
  domain: 'http://www.vyan.top/',
}
