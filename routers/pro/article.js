const express = require('express')
const controller = require('root/app/controller/pro')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/', reqHandler(controller.article.findAll))
  .get('/:id', reqHandler(controller.article.findOne))
  .post('/', reqHandler(controller.article.create))
  .patch('/:id', reqHandler(controller.article.update))
  .delete('/:id', reqHandler(controller.article.remove))

module.exports = router
