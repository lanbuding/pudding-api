const express = require('express')
const controller = require('root/app/controller/pro')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/', reqHandler(controller.banner.findAll))
  .get('/:id', reqHandler(controller.banner.findOne))
  .post('/', reqHandler(controller.banner.create))
  .patch('/:id', reqHandler(controller.banner.update))
  .delete('/:id', reqHandler(controller.banner.remove))

module.exports = router
