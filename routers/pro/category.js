const express = require('express')
const controller = require('root/app/controller/pro')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/', reqHandler(controller.category.findAll))
  .get('/:id', reqHandler(controller.category.findOne))
  .post('/', reqHandler(controller.category.create))
  .patch('/:id', reqHandler(controller.category.update))
  .delete('/:id', reqHandler(controller.category.remove))

module.exports = router
