const express = require('express')
const controller = require('root/app/controller/pro')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/', reqHandler(controller.comment.findAll))
  .get('/:id', reqHandler(controller.comment.findOne))
  .post('/', reqHandler(controller.comment.create))
  .patch('/:id', reqHandler(controller.comment.update))
  .delete('/:id', reqHandler(controller.comment.remove))

module.exports = router
