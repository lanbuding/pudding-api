const express = require('express')
const controller = require('root/app/controller/pro')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .post('/login', reqHandler(controller.user.login))
  .post('/register', reqHandler(controller.user.register))
  .get('/', reqHandler(controller.user.findAll))
  .get('/:id', reqHandler(controller.user.findOne))
  .post('/', reqHandler(controller.user.create))
  .patch('/:id', reqHandler(controller.user.update))
  .delete('/:id', reqHandler(controller.user.remove))
  .patch('/password/:id', reqHandler(controller.user.changePassword))

module.exports = router
