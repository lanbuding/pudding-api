const express = require('express')
const controller = require('root/app/controller/pro')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/', reqHandler(controller.link.findAll))
  .get('/:id', reqHandler(controller.link.findOne))
  .post('/', reqHandler(controller.link.create))
  .patch('/:id', reqHandler(controller.link.update))
  .delete('/:id', reqHandler(controller.link.remove))

module.exports = router
