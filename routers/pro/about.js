const express = require('express')
const controller = require('root/app/controller/pro')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/', reqHandler(controller.about.findAll))
  .get('/:id', reqHandler(controller.about.findOne))
  .post('/', reqHandler(controller.about.create))
  .patch('/:id', reqHandler(controller.about.update))
  .delete('/:id', reqHandler(controller.about.remove))

module.exports = router
