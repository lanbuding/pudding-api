const express = require('express')
const controller = require('root/app/controller/pro')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/', reqHandler(controller.system.findAll))
  .get('/:id', reqHandler(controller.system.findOne))
  .post('/', reqHandler(controller.system.create))
  .patch('/:id', reqHandler(controller.system.update))
  .delete('/:id', reqHandler(controller.system.remove))

module.exports = router
