const express = require('express')
const controller = require('root/app/controller/pro')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/', reqHandler(controller.contact.findAll))
  .get('/:id', reqHandler(controller.contact.findOne))
  .post('/', reqHandler(controller.contact.create))
  .patch('/:id', reqHandler(controller.contact.update))
  .delete('/:id', reqHandler(controller.contact.remove))

module.exports = router
