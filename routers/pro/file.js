const express = require('express')
const controller = require('root/app/controller/pro')
const { reqHandler } = require('root/utils')
const router = express.Router()

// 上传
router
  .post('/upload', reqHandler(controller.file.upload))

module.exports = router
