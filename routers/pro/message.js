const express = require('express')
const controller = require('root/app/controller/pro')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/', reqHandler(controller.message.findAll))
  .get('/:id', reqHandler(controller.message.findOne))
  .post('/', reqHandler(controller.message.create))
  .patch('/:id', reqHandler(controller.message.update))
  .delete('/:id', reqHandler(controller.message.remove))

module.exports = router
