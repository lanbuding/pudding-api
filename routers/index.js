const express = require('express')
const router = express.Router()

router.get('/test', function(req, res, next) {
  const user = {
    name: 'jikey223333',
    age: 20,
    address: 'zh',
  }

  res.json({ code: 0, data: user, message: 'success' })
})

// 上传
router.use('/api/file', require('./pro/file'))

// 后台路由
// 用户管理
router.use('/api/pro/user', require('./pro/user'))
// 文章管理
router.use('/api/pro/article', require('./pro/article'))
// 分类管理
router.use('/api/pro/category', require('./pro/category'))
// 留言管理
router.use('/api/pro/message', require('./pro/message'))
// 评论管理
router.use('/api/pro/comment', require('./pro/comment'))
// 系统管理
router.use('/api/pro/system', require('./pro/system'))
// 关于我们
router.use('/api/pro/about', require('./pro/about'))
// Banner管理
router.use('/api/pro/banner', require('./pro/banner'))
// 友链管理
router.use('/api/pro/link', require('./pro/link'))
// 联系我们
router.use('/api/pro/contact', require('./pro/contact'))

// 前台路由
// 首页
router.use('/api/web/home', require('./web/home'))
// 文章
router.use('/api/web/article', require('./web/article'))
// 评论
router.use('/api/web/comment', require('./web/comment'))
// 留言
router.use('/api/web/message', require('./web/message'))
// 关于
router.use('/api/web/about', require('./web/about'))
// 公共
router.use('/api/web/common', require('./web/common'))
// 联系我们
router.use('/api/web/contact', require('./web/contact'))


module.exports = router
