const express = require('express')
const controller = require('root/app/controller/web')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/', reqHandler(controller.comment.findAll))
  .get('/:id', reqHandler(controller.comment.findOne))
  .get('/article/:id', reqHandler(controller.comment.findOneByArticleId))
  .post('/', reqHandler(controller.comment.create))

module.exports = router
