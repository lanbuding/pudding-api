const express = require('express')
const controller = require('root/app/controller/web')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/', reqHandler(controller.message.findAll))
  .post('/', reqHandler(controller.message.create))

module.exports = router
