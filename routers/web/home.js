const express = require('express')
const controller = require('root/app/controller/web')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/search', reqHandler(controller.home.findAll))
  .get('/article/:type', reqHandler(controller.home.findArticle))
  .get('/banner', reqHandler(controller.home.findBanner))
  .get('/banner/m/:id', reqHandler(controller.home.findBanner))
  .get('/user/:id', reqHandler(controller.home.findUserInfo))
  .get('/category', reqHandler(controller.home.findCategory))
  .get('/link', reqHandler(controller.home.findLink))

module.exports = router
