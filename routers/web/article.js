const express = require('express')
const controller = require('root/app/controller/web')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/category', reqHandler(controller.article.findCategory))
  .get('/:id', reqHandler(controller.article.findOne))
  .post('/like', reqHandler(controller.article.like))

module.exports = router
