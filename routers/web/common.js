const express = require('express')
const controller = require('root/app/controller/web')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/captcha', reqHandler(controller.common.captcha))

module.exports = router
