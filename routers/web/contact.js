const express = require('express')
const controller = require('root/app/controller/web')
const { reqHandler } = require('root/utils')
const router = express.Router()

router
  .get('/', reqHandler(controller.contact.findAll))
  .get('/:id', reqHandler(controller.contact.findOne))

module.exports = router
